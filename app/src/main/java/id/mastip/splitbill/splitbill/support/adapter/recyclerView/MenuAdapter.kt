package id.mastip.splitbill.splitbill.support.adapter.recyclerView

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.MenuModel
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder.MenuViewHolder

class MenuAdapter(private val list: List<MenuModel>, private val listener: OnItemClickedListener<MenuModel>) : RecyclerView.Adapter<MenuViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MenuViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.view_holder_menu, p0, false)
        return MenuViewHolder(view, listener)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: MenuViewHolder, p1: Int) {
        p0.setUpModelToView(list[p1])
    }

}