package id.mastip.splitbill.splitbill.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderModel(
    var idUser: Int? = 0,
    var id: Int? = 0,
    var nama: String? = "",
    var price: Int? = 0,
    var isMyTag: Boolean? = false): Parcelable
