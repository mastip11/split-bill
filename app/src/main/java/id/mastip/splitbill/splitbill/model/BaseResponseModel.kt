package id.mastip.splitbill.splitbill.model

data class BaseResponseModel(
    var responseCode: Int = 200,
    var message: String = "",
    var responseValue: String = ""
)