package id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.VoucherModel

class VoucherViewHolder(view: View): RecyclerView.ViewHolder(view) {

    @BindView(R.id.voucher_image_view)
    protected lateinit var voucherImageView: ImageView

    @BindView(R.id.voucher_name_text_view)
    protected lateinit var voucherNameTextView: TextView

    @BindView(R.id.voucher_price_text_view)
    protected lateinit var priceImageView: TextView

    init {
        ButterKnife.bind(this, view)
    }

    fun setUpModelToUI(model: VoucherModel) {
        voucherNameTextView.text = model.title
        priceImageView.text = "Rp. ${model.price}"
    }

}