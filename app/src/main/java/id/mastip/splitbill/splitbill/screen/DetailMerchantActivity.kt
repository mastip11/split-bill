package id.mastip.splitbill.splitbill.screen

import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.screen.presenter.DetailMerchantController
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivityWithoutInjection

class DetailMerchantActivity: CustomBaseActivityWithoutInjection() {

    private lateinit var presender: DetailMerchantController

    override fun getLayoutId(): Int = R.layout.activity_detail_merchant

    override fun initializeDefaultValue() {
        super.initializeDefaultValue()
        presender = DetailMerchantController(this)
    }
}