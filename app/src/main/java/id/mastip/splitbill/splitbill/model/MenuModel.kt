package id.mastip.splitbill.splitbill.model

class MenuModel {

    var id: Int = 0
    var name: String = ""
    var imageId: Int = 0

    constructor()

    constructor(id: Int, name: String, imageId: Int) {
        this.id = id
        this.name = name
        this.imageId = imageId
    }
}