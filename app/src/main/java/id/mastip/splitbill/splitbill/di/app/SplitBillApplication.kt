package id.mastip.splitbill.splitbill.di.app

import android.app.Activity
import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import id.mastip.splitbill.splitbill.di.component.DaggerSplitBillComponent
import javax.inject.Inject

class SplitBillApplication: Application(), HasActivityInjector {

    @Inject
    lateinit var activityAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        DaggerSplitBillComponent.builder().apps(this).build().inject(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityAndroidInjector
    }
}