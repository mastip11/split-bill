package id.mastip.splitbill.splitbill.di.module.order

import dagger.Module
import dagger.Provides
import id.mastip.splitbill.splitbill.screen.OrderActivity
import id.mastip.splitbill.splitbill.screen.presenter.OrderPresenter

@Module
class OrderModule {

    @Provides
    fun providesPresenter(activity: OrderActivity): OrderPresenter {
        return OrderPresenter(activity)
    }
}