package id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.squareup.picasso.Picasso
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.MenuModel
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener

class MenuViewHolder(view: View, private val listener: OnItemClickedListener<MenuModel>) : RecyclerView.ViewHolder(view) {

    @BindView(R.id.viewHolderPlaceHolderImageView)
    protected lateinit var placeHolderImageView: ImageView

    @BindView(R.id.viewHolderPlaceHolderTextView)
    protected lateinit var placeHolderTextView: TextView

    private lateinit var model: MenuModel

    init {
        ButterKnife.bind(this, view)
    }

    fun setUpModelToView(model: MenuModel) {
        this.model = model
        Picasso.get().load(model.imageId).into(placeHolderImageView)
        placeHolderTextView.text = model.name
    }

    @OnClick
    protected fun onBaseClicked() {
        listener.onItemClickedListener(model)
    }
}