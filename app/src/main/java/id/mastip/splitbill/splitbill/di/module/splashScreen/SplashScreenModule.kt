package id.mastip.splitbill.splitbill.di.module.splashScreen

import dagger.Module
import dagger.Provides
import id.mastip.splitbill.splitbill.screen.SplashScreenActivity
import id.mastip.splitbill.splitbill.screen.presenter.SplashScreenPresenter

@Module
class SplashScreenModule {

    @Provides
    fun providesSplashScreenPresenter(splashScreenActivity: SplashScreenActivity): SplashScreenPresenter {
        return SplashScreenPresenter(splashScreenActivity)
    }
}