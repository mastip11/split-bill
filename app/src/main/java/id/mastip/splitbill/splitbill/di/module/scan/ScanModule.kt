package id.mastip.splitbill.splitbill.di.module.scan

import dagger.Module
import dagger.Provides
import id.mastip.splitbill.splitbill.screen.ScanActivity
import id.mastip.splitbill.splitbill.screen.presenter.ScanPresenter

@Module
class ScanModule {

    @Provides
    fun provideScanPresenter(scanActivity: ScanActivity): ScanPresenter {
        return ScanPresenter(scanActivity)
    }
}