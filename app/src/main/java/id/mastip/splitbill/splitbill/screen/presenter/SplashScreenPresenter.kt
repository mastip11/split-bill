package id.mastip.splitbill.splitbill.screen.presenter

import id.mastip.splitbill.splitbill.screen.SplashScreenActivity

class SplashScreenPresenter(private val splashScreenActivity: SplashScreenActivity) {

    init {
        counterToNextActivity()
    }

    private fun counterToNextActivity() {
        val thread = Thread(Runnable {
            try {
                Thread.sleep(2000)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                splashScreenActivity.moveToLogin()
            }
        })
        thread.start()
    }
}