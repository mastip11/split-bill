package id.mastip.splitbill.splitbill.di.module.home

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.mastip.splitbill.splitbill.screen.HomeActivity

@Module
abstract class HomeBindModule {

    @ContributesAndroidInjector(modules = [HomeModule::class])
    abstract fun contributeHomeActivity(): HomeActivity
}