package id.mastip.splitbill.splitbill.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import id.mastip.splitbill.splitbill.di.app.SplitBillApplication
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.navigator.Navigate
import javax.inject.Singleton

@Module
class SplitBillModules {

    @Provides
    @Singleton
    fun provideNavigator(): Navigate {
        return Navigate()
    }

    @Provides
    @Singleton
    fun provideSplitBillApplication(): SplitBillApplication {
        return SplitBillApplication()
    }

    @Provides
    @Singleton
    fun provideSplitBillSharedPreferences(context: Context): DataCenterManager {
        return DataCenterManager(context)
    }
}