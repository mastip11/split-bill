package id.mastip.splitbill.splitbill.di.module.order

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.mastip.splitbill.splitbill.screen.OrderActivity

@Module
abstract class OrderBindModule {

    @ContributesAndroidInjector(modules = [OrderModule::class])
    abstract fun contributeOrderActivity(): OrderActivity
}