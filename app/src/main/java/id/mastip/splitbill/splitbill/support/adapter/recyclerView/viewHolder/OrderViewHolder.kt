package id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener

class OrderViewHolder(view: View, private val listener: OnItemClickedListener<OrderModel>) : RecyclerView.ViewHolder(view){

    @BindView(R.id.custom_transaction_name)
    protected lateinit var tvName: TextView

    @BindView(R.id.custom_transaction_date)
    protected lateinit var tvPrice: TextView

    @BindView(R.id.custom_transaction_image)
    protected lateinit var ivImage: ImageView

    @BindView(R.id.custom_transaction_tag)
    protected lateinit var taggedByTextView: TextView

    private lateinit var data: OrderModel

    init {
        ButterKnife.bind(this, view)
    }

    fun setUpToUI(data: OrderModel){
        this.data = data;
        ivImage.setImageResource(R.drawable.ic_food)
        tvName.text = data.nama
        tvPrice.text = "Rp. ${data.price}"

        if (data.idUser == 0) {
            itemView.alpha = 1f
        } else {
            itemView.alpha = 0.4f
        }

        taggedByTextView.visibility = View.VISIBLE
        taggedByTextView.text = "Tagged by ${data.idUser}"
    }

    @OnClick
    protected fun onBaseClicked(){
        if (data.idUser == 0 || data.idUser == DataCenterManager.getSingleton(itemView.context).id) {
            listener.onItemClickedListener(data)
        }
    }
}
