package id.mastip.splitbill.splitbill.manager.connection.listener

interface DataReturnListener<T> {
    fun onSuccess(message: String, data: T)
    fun onFailed(message: String)
}