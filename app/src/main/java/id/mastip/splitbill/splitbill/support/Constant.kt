package id.mastip.splitbill.splitbill.support

object Constant {

    const val REQUEST_CODE_GOOGLE_SIGN_IN = 401

    const val KEY_NAME = "name"
    const val KEY_EMAIL = "email"
    const val KEY_PHOTO = "photo"
    const val KEY_PHOTO_URL = "photo_url"
    const val KEY_DATA = "data"
    const val KEY_ID = "id"
    const val KEY_PRICE = "price"
    const val KEY_IS_MY_TAG = "is_my_tag"
    const val KEY_FIREBASE = " firebase"
    const val KEY_GOOGLE_ID = "google_id"
    const val KEY_RESULT_CODE = "result_code"
    const val KEY_MESSAGE = "message"
    const val KEY_VALUES = "values"
    const val KEY_FCM_ID = "fcm_id"
    const val KEY_MERCHANT_NAME = "merchant_name"
    const val KEY_CREATED_AT = "created_at"
    const val KEY_TRANSACTION_ID = "transaction_id"
    const val KEY_USER_ID = "user_id"
    const val KEY_USER_TAG_ID = "user_tag_id"
    const val KEY_DATA_BARCODE = "data_barcode"

    const val REQUEST_CODE_LOCATION_PERMISSION = 300

    const val ACTION_UPDATE_ORDER = "update_order"

    val DEFAULT_LONGITUDE: Double = 106.826620
    val DEFAULT_LATITUDE: Double = -6.164420
    val DEFAULT_LOCATION: String = "Kebon Kelapa"

    const val UPDATE_MENU = "update_menu"

    const val CODE_ERROR_CONNECTION = 4004
    const val CODE_ERROR_UNAUTHENTICATE = 4005
    const val CODE_ERROR_SERVER = 4006
    const val CODE_ERROR_NETWORK = 4007
    const val CODE_ERROR_PARSE = 4008
    const val CODE_ERROR_PARSE_SERVER = 409
    const val CODE_ERROR_UNDEFINED = 4010
}