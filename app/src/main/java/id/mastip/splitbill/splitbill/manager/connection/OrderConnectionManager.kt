package id.mastip.splitbill.splitbill.manager.connection

import android.content.Context
import id.mastip.splitbill.splitbill.manager.connection.listener.BasicListener
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.model.BaseResponseModel
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.support.Constant
import id.mastip.splitbill.splitbill.manager.connection.TransactionConnectionManager
import id.mastip.splitbill.splitbill.manager.connection.listener.ConnectionListener
import id.mastip.splitbill.splitbill.support.ConnectionConstant
import id.mastip.splitbill.splitbill.support.UIHelper
import org.json.JSONArray
import org.json.JSONObject

object OrderConnectionManager: BaseConnectionManager() {

    fun getSingleton(context: Context): OrderConnectionManager {
        OrderConnectionManager.parentSingleton(context)
        return this
    }

    fun updateUserTagId(transactionId: Int, userId: Int, listener: ConnectionListener) {
        val json = JSONObject()
        json.put(Constant.KEY_ID, transactionId)
        json.put(Constant.KEY_USER_ID, userId)

        requestPOST(ConnectionConstant.UPDATE_ORDER_ITEM, json, object : BasicListener {
            override fun onResponse(model: BaseResponseModel) {
                listener.onSuccess(model.message)
            }

            override fun onFailed(errorCode: Int, message: String) {
                listener.onFailed(message)
            }
        })
    }

    fun getAllOrderModel(id: Int, listener: DataReturnListener<MutableList<OrderModel>>) {
        val json = JSONObject()
        json.put(Constant.KEY_TRANSACTION_ID, id)
        requestPOST(ConnectionConstant.GET_ORDER, json, object : BasicListener {

            val transactionList: MutableList<OrderModel> = ArrayList()

            override fun onResponse(model: BaseResponseModel) {
                val jsonArray = JSONArray(model.responseValue)
                for (a in 0 until jsonArray.length()) {
                    val objectTransaction = jsonArray.getJSONObject(a)

                    val orderModel = OrderModel()
                    orderModel.id = objectTransaction.getInt(Constant.KEY_ID)
                    if (objectTransaction.has(Constant.KEY_NAME)) {
                        orderModel.nama = objectTransaction.getString(Constant.KEY_NAME)
                    }
                    orderModel.idUser = objectTransaction.getInt(Constant.KEY_USER_TAG_ID)
                    orderModel.price = objectTransaction.getInt(Constant.KEY_PRICE)

                    transactionList.add(orderModel)
                }

                listener.onSuccess(model.message, transactionList)
            }

            override fun onFailed(errorCode: Int, message: String) {
                listener.onFailed(message)
            }
        })
    }

}