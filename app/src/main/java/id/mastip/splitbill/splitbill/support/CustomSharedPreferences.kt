package id.mastip.splitbill.splitbill.support

import android.content.SharedPreferences

fun SharedPreferences.getStringNonNullable(key: String, defaultValue: String? = "") = getString(key, defaultValue)