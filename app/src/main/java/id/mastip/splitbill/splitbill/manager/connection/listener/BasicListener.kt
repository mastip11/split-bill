package id.mastip.splitbill.splitbill.manager.connection.listener

import id.mastip.splitbill.splitbill.model.BaseResponseModel

interface BasicListener {
    fun onResponse(model: BaseResponseModel)
    fun onFailed(errorCode: Int, message: String)
}