package id.mastip.splitbill.splitbill.di.module.login

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.mastip.splitbill.splitbill.screen.LoginActivity

@Module
abstract class LoginBindModule {

    @ContributesAndroidInjector(modules = [LoginModule::class])
    abstract fun contributeLoginActivity(): LoginActivity
}