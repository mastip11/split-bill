package id.mastip.splitbill.splitbill.support.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import butterknife.ButterKnife
import id.mastip.splitbill.splitbill.support.dialog.LoadingDialog

abstract class CustomBaseActivityWithoutInjection : AppCompatActivity() {

    private var dialog: LoadingDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        ButterKnife.bind(this)
        hideActionBar()
        initializeDefaultValue()
    }

    private fun hideActionBar() {
        supportActionBar.let {
            it?.hide()
        }
    }

    fun showLoading() {
        if (dialog == null) {
            dialog = LoadingDialog(this)
        }
        dialog?.show()
    }

    fun dismissLoading() {
        dialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

    open fun initializeDefaultValue() {

    }

    abstract fun getLayoutId(): Int
}