package id.mastip.splitbill.splitbill.screen.presenter

import id.mastip.splitbill.splitbill.model.MenuModel
import id.mastip.splitbill.splitbill.screen.HomeActivity
import id.mastip.splitbill.splitbill.support.Generator

class HomePresenter(private val homeActivity: HomeActivity) {

    private var list: List<MenuModel> = Generator.generateMenuModels()

    fun setUpAdapter() {
        homeActivity.setUpAdapter(list)
    }

    fun onItemClicked(menuModel: MenuModel) {
        when (menuModel.id) {
            1 -> homeActivity.goToScanActivity()
            2 -> homeActivity.goToHistory()
            3 -> homeActivity.logout()
            4 -> homeActivity.goToVoucher()
            5 -> homeActivity.goToMerchant()
        }
    }
}