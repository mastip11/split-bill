package id.mastip.splitbill.splitbill.screen.presenter

import android.widget.Toast
import com.google.android.gms.vision.barcode.Barcode
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.connection.OrderConnectionManager
import id.mastip.splitbill.splitbill.manager.connection.listener.ConnectionListener
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.DetailTransactionActivity
import id.mastip.splitbill.splitbill.support.Constant
import id.mastip.splitbill.splitbill.support.Generator

class DetailTransactionPresenter(private val activity: DetailTransactionActivity) {

    protected lateinit var model: TransactionModel
    protected lateinit var barcodeData: Barcode
    private var list: MutableList<OrderModel> = ArrayList()

    init {
        activity.intent.extras?.let {
            it.apply {
                if (it.containsKey(Constant.KEY_DATA_BARCODE)) {
                    getParcelable<Barcode>(Constant.KEY_DATA_BARCODE)?.let { barcode ->
                        barcodeData = barcode

                        model = TransactionModel()
                        model.id = barcode.displayValue.toInt()
                    }
                }
                if (it.containsKey(Constant.KEY_DATA)) {
                    getParcelable<TransactionModel>(Constant.KEY_DATA)?.let { transactionModel ->
                        model = transactionModel
                    }
                }
            }
        }

        activity.setUpUI(model)
        activity.setAdapter(list)

        getOrderData()
    }

    fun showBarcodeClicked() {
        activity.showBarcodeDialog(model.id.toString())
    }

    fun getOrderData() {
        activity.showLoading()
        OrderConnectionManager.getSingleton(activity).getAllOrderModel(model.id!!, object : DataReturnListener<MutableList<OrderModel>> {
            override fun onSuccess(message: String, data: MutableList<OrderModel>) {
                activity.dismissLoading()
                this@DetailTransactionPresenter.list.clear()
                this@DetailTransactionPresenter.list.addAll(data)

                activity.adapter.notifyDataSetChanged()
            }

            override fun onFailed(message: String) {
                activity.dismissLoading()
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun onItemClickedListener(model: OrderModel) {
        if (model.idUser!! == 0) {
            model.idUser = DataCenterManager.getSingleton(activity).id
        } else {
            model.idUser = 0
        }

        sendTagToServer(model.id!!, model.idUser!!)
    }

    fun sendTagToServer(id: Int, userId: Int) {
        activity.showLoading()
        OrderConnectionManager.getSingleton(activity).updateUserTagId(id, userId, object : ConnectionListener {
            override fun onSuccess(message: String) {
                activity.dismissLoading()

                for (a in 0 until list.size) {
                    if (list[a].id == id) {
                        list[a].idUser = userId
                        break
                    }
                }

                activity.adapter.notifyDataSetChanged()
            }

            override fun onFailed(message: String) {
                activity.dismissLoading()
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}