package id.mastip.splitbill.splitbill.manager

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import id.mastip.splitbill.splitbill.support.Constant
import id.mastip.splitbill.splitbill.support.getStringNonNullable

class DataCenterManager(context: Context) {

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var editor: SharedPreferences.Editor

    init {
        initiateSharedPreference(context)
    }

    var id: Int
        get() = sharedPreferences.getInt(Constant.KEY_ID, 0)
        set(id) {
            if (!::editor.isInitialized) {
                editor = sharedPreferences.edit()
            }
            editor.putInt(Constant.KEY_ID, id)
            editor.apply()
        }

    var name: String
        get() = sharedPreferences.getStringNonNullable(Constant.KEY_NAME)
        set(name) {
            if (!::editor.isInitialized) {
                editor = sharedPreferences.edit()
            }
            editor.putString(Constant.KEY_NAME, name)
            editor.apply()
        }

    var email: String
        get() = sharedPreferences.getStringNonNullable(Constant.KEY_EMAIL)
        set(email) {
            if (!::editor.isInitialized) {
                editor = sharedPreferences.edit()
            }
            editor.putString(Constant.KEY_EMAIL, email)
            editor.apply()
        }

    var photoUrl: String
        get() = sharedPreferences.getStringNonNullable(Constant.KEY_PHOTO)
        set(photoUrl) {
            if (!::editor.isInitialized) {
                editor = sharedPreferences.edit()
            }
            editor.putString(Constant.KEY_PHOTO, photoUrl)
            editor.apply()
        }

    var googleId: String
        get() = sharedPreferences.getStringNonNullable(Constant.KEY_GOOGLE_ID)
        set(googleId) {
            if (!::editor.isInitialized) {
                editor = sharedPreferences.edit()
            }
            editor.putString(Constant.KEY_GOOGLE_ID, googleId)
            editor.apply()
        }

    var firebaseId: String
        get() = sharedPreferences.getStringNonNullable(Constant.KEY_FIREBASE)
        set(firebaseId) {
            if (!::editor.isInitialized) {
                editor = sharedPreferences.edit()
            }
            editor.putString(Constant.KEY_FIREBASE, firebaseId)
            editor.apply()
        }

    private fun initiateSharedPreference(context: Context) {
        if (!::sharedPreferences.isInitialized) {
            sharedPreferences = context.getSharedPreferences("split_bill", MODE_PRIVATE)
        }
    }

    fun clearAllData() {
        if (!::editor.isInitialized) {
            editor = sharedPreferences.edit()
        }
        editor.clear()
        editor.apply()
    }

    companion object {
        private lateinit var singleton: DataCenterManager

        @JvmStatic fun getSingleton(context: Context): DataCenterManager {
            singleton = DataCenterManager(context)
            return singleton
        }
    }
}