package id.mastip.splitbill.splitbill.di.component

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import id.mastip.splitbill.splitbill.di.app.SplitBillApplication
import id.mastip.splitbill.splitbill.di.module.SplitBillModules
import id.mastip.splitbill.splitbill.di.module.history.HistoryBindModule
import id.mastip.splitbill.splitbill.di.module.home.HomeBindModule
import id.mastip.splitbill.splitbill.di.module.login.LoginBindModule
import id.mastip.splitbill.splitbill.di.module.order.OrderBindModule
import id.mastip.splitbill.splitbill.di.module.scan.ScanBindModule
import id.mastip.splitbill.splitbill.di.module.splashScreen.SplashScreenBindModule
import id.mastip.splitbill.splitbill.di.module.voucher.VoucherBindModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    SplitBillModules::class,
    SplashScreenBindModule::class,
    LoginBindModule::class,
    HomeBindModule::class,
    ScanBindModule::class,
    HistoryBindModule::class,
    VoucherBindModule::class,
    OrderBindModule::class,
    AndroidSupportInjectionModule::class
])
interface SplitBillComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun apps(application: SplitBillApplication): Builder
        fun build(): SplitBillComponent
    }

    fun inject(splitBillApplication: SplitBillApplication)
}