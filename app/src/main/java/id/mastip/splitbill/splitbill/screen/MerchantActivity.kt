package id.mastip.splitbill.splitbill.screen

import android.content.Intent
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import butterknife.BindView
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.manager.google.MapsManager
import id.mastip.splitbill.splitbill.manager.google.listener.MapsManagerListener
import id.mastip.splitbill.splitbill.model.MerchantModel
import id.mastip.splitbill.splitbill.screen.presenter.MerchantPresenter
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.MarchantAdapter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivityWithoutInjection

class MerchantActivity: CustomBaseActivityWithoutInjection() {

    @BindView(R.id.transaction_list)
    protected lateinit var listOrder: RecyclerView

    private lateinit var mapsManager: MapsManager
    private lateinit var presenter: MerchantPresenter
    lateinit var adapter: MarchantAdapter

    private val mapsManagerListener = object : MapsManagerListener {
        override fun onPermissionDenied() {
            Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT).show()
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_transaction

    override fun initializeDefaultValue() {
        super.initializeDefaultValue()
        presenter = MerchantPresenter(this)
        configureMaps()
    }

    private fun configureMaps() {
        mapsManager = MapsManager(this, R.id.locationPickerFragment)
        mapsManager.setMapsManagerListener(mapsManagerListener)
        mapsManager.onClickButtonLocation()
        mapsManager.isAllowToMove = true
    }

    fun configureRecyclerView(list: List<MerchantModel>) {
        adapter = MarchantAdapter(list)

        listOrder.layoutManager = LinearLayoutManager(this)
        listOrder.itemAnimator = DefaultItemAnimator()
        listOrder.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        mapsManager.onStart()
    }

    override fun onStop() {
        super.onStop()
        mapsManager.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        mapsManager.onActivityResult(requestCode, resultCode, data)
    }
}