package id.mastip.splitbill.splitbill.screen.presenter

import android.widget.Toast
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.iid.FirebaseInstanceId
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.connection.UserConnectionManager
import id.mastip.splitbill.splitbill.manager.connection.listener.ConnectionListener
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.screen.LoginActivity

class LoginPresenter(private val loginActivity: LoginActivity) {

    init {
        if (DataCenterManager.getSingleton(loginActivity).id != 0) {
            loginActivity.goToHomeScreen()
        }
    }

    fun handleGoogleSignIn(googleSignInAccount: GoogleSignInAccount?) {
        googleSignInAccount?.let {
            var name: String = ""
            var email: String = ""
            var photoUrl: String = ""
            var googleId: String = ""
            var firebaseId: String = ""
            it.displayName?.let { displayName ->
                name = displayName
                DataCenterManager.getSingleton(loginActivity).name = displayName
            }

            it.email?.let { googleEmail ->
                email = googleEmail
                DataCenterManager.getSingleton(loginActivity).email = googleEmail
            }
            it.photoUrl?.let { uri ->
                photoUrl = uri.toString()
                DataCenterManager.getSingleton(loginActivity).photoUrl = uri.toString()
            }
            it.id?.let { id ->
                googleId = id
                DataCenterManager.getSingleton(loginActivity).googleId = id
            }

            firebaseId = FirebaseInstanceId.getInstance().token!!
            DataCenterManager.getSingleton(loginActivity).firebaseId = FirebaseInstanceId.getInstance().token!!

            loginActivity.showLoading()
            UserConnectionManager.getSingleton(loginActivity).login(name, email, photoUrl, googleId, firebaseId, object : DataReturnListener<Int> {
                override fun onSuccess(message: String, data: Int) {
                    DataCenterManager.getSingleton(loginActivity).id = data

                    loginActivity.dismissLoading()
                    loginActivity.goToHomeScreen()
                }

                override fun onFailed(message: String) {
                    loginActivity.dismissLoading()
                    Toast.makeText(loginActivity, message, Toast.LENGTH_LONG).show()
                }
            })
        }
    }
}