package id.mastip.splitbill.splitbill.di.module.history

import dagger.Module
import dagger.Provides
import id.mastip.splitbill.splitbill.screen.HistoryActivity
import id.mastip.splitbill.splitbill.screen.presenter.HistoryPresenter

@Module
class HistoryModule {

    @Provides
    fun provideHistoryPresenter(historyActivity: HistoryActivity): HistoryPresenter {
        return HistoryPresenter(historyActivity)
    }
}