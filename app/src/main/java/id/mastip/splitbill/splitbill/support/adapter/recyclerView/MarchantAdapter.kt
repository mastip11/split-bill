package id.mastip.splitbill.splitbill.support.adapter.recyclerView

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.MerchantModel
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder.HeaderViewHolder
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder.MerchantViewHolder

class MarchantAdapter(private var list: List<MerchantModel>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    val TYPE_HEADER = 1
    val TYPE_CONTENT = 2

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            TYPE_HEADER
        } else {
            TYPE_CONTENT
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        if (p1 == TYPE_HEADER) {
            return HeaderViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.adapter_header, p0, false))
        }
        return MerchantViewHolder(LayoutInflater.from(p0.context).inflate(R.layout.adapter_merchant, p0, false))
    }

    override fun getItemCount(): Int = list.size + 1

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p1 == 0) {
            (p0 as HeaderViewHolder).setUpModelView("")
            return
        }
        (p0 as MerchantViewHolder).setUpModelToView(list[p1])
    }
}