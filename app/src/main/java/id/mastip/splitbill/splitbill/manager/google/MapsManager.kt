package id.mastip.splitbill.splitbill.manager.google

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.util.Log
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResultCallback
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapFragment
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.manager.google.listener.MapsManagerListener
import id.mastip.splitbill.splitbill.support.Constant
import id.mastip.splitbill.splitbill.support.UIHelper

class MapsManager(private var activity: Activity, mapsLayout: Int) {

    private lateinit var googleApiClient: GoogleApiClient
    private lateinit var locationRequest: LocationRequest
    private lateinit var googleMap: GoogleMap
    private lateinit var mapsManagerListener: MapsManagerListener

    private var address: String = Constant.DEFAULT_LOCATION
    var longitude: Double = Constant.DEFAULT_LONGITUDE
    var latitude: Double = Constant.DEFAULT_LATITUDE
    private var isOnDrag = false
    private var isSelectLocation = false
    var isAllowToMove = false

    private var baseImageID = R.drawable.ic_location

    private val locationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            if (!checkPermission()) {
                return
            }

            googleApiClient.let {
                if (googleApiClient.isConnected) {
                    LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this)
                }
            }

            latitude = location.latitude
            longitude = location.longitude

            onMovingCamera(LatLng(location.latitude, location.longitude), true)
        }
    }

    private val resultLocationSettingsCallback = ResultCallback<LocationSettingsResult> { locationSettingsResult ->
        val status = locationSettingsResult.status
        when (status.statusCode) {
            LocationSettingsStatusCodes.SUCCESS -> requestCurrentLocation()
            LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> showChangeSettingsDetail(status)
            LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> Log.v("", "change unavailable")
        }
    }

    private val onMapReadyCallback = OnMapReadyCallback { googleMap ->
        this@MapsManager.googleMap = googleMap
        configureCurrentLocation()
    }

    private val onMarkerClickListener = GoogleMap.OnMarkerClickListener {
        isOnDrag = false

        updateMarker(R.drawable.ic_location)

        true
    }

    private val onCameraChangeListener = GoogleMap.OnCameraChangeListener { cameraPosition ->
        val location = cameraPosition.target

        isSelectLocation = true

        var imageId = baseImageID
        if (!isOnDrag) {
            imageId = R.drawable.ic_location
            isOnDrag = true
        }

        longitude = location.longitude
        latitude = location.latitude

        if (longitude != 0.0 && latitude != 0.0) {
            updateMarker(imageId)
        }
    }

    private val onMyLocationButtonClickListener = GoogleMap.OnMyLocationButtonClickListener {
        isSelectLocation = true
        onClickButtonLocation()
        true
    }

    init {
        val mapFragment = activity.fragmentManager.findFragmentById(mapsLayout) as MapFragment
        mapFragment.getMapAsync(onMapReadyCallback)
        buildGoogleApiClient(activity)
    }

    private fun configureCurrentLocation() {
        if (!checkPermission()) {
            return
        }

        googleMap.isMyLocationEnabled = true
        googleMap.setOnCameraChangeListener(onCameraChangeListener)
        googleMap.uiSettings.isZoomControlsEnabled = false
        googleMap.uiSettings.isZoomGesturesEnabled = true
        googleMap.uiSettings.isScrollGesturesEnabled = isAllowToMove
        googleMap.setOnMarkerClickListener(onMarkerClickListener)
        googleMap.setOnMyLocationButtonClickListener(onMyLocationButtonClickListener)

        isOnDrag = false
        onMovingCamera(googleMap.cameraPosition.target, true)

        if (longitude != 0.toDouble() || latitude != 0.toDouble()) {
            onMovingCamera(LatLng(latitude, longitude), true)
        } else {
            requestCurrentLocation()
        }
    }

    private fun buildGoogleApiClient(context: Context) {
        googleApiClient = GoogleApiClient.Builder(context)
            .addApi(LocationServices.API)
            .build()

        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 100
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 300 && resultCode == Activity.RESULT_OK) {
            requestCurrentLocation()
        } else if (requestCode == 300 && resultCode == 0) {
            onMovingCamera(LatLng(latitude, longitude), true)
        }
    }

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == Constant.REQUEST_CODE_LOCATION_PERMISSION && permissions.isNotEmpty()) {
            var isGranted = false
            if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION, ignoreCase = true) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isGranted = checkPermission()
            } else if (permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION, ignoreCase = true) && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                isGranted = true
            }

            if (isGranted) {
                configureCurrentLocation()
                buildGoogleApiClient(activity)
                onClickButtonLocation()
            } else {
                mapsManagerListener.let {
                    mapsManagerListener.onPermissionDenied()
                }
            }
        }
    }

    fun onStart() {
        googleApiClient.connect()
    }

    fun onStop() {
        googleApiClient.disconnect()
    }

    fun onClickButtonLocation() {
        val locationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest).build()
        val pendingResult = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, locationSettingsRequest)
        pendingResult.setResultCallback(resultLocationSettingsCallback)
    }

    private fun onMovingCamera(location: LatLng, isNeedGetLocationName: Boolean) {
        if (location.latitude == 0.0 && location.longitude == 0.0) {
            return
        }

        googleMap.let {
            val cameraUpdateLocation = CameraUpdateFactory.newLatLngZoom(location, 15f)
            googleMap.animateCamera(cameraUpdateLocation)
        }
    }

    private fun showChangeSettingsDetail(status: Status) {
        try {
            status.startResolutionForResult(activity, Constant.REQUEST_CODE_LOCATION_PERMISSION)
        } catch (e: IntentSender.SendIntentException) {
            e.printStackTrace()
        }

    }

    private fun updateMarker(imageId: Int) {
        val location = LatLng(latitude, longitude)

        val marker = MarkerOptions()
        marker.position(location)
        marker.icon(BitmapDescriptorFactory.fromResource(imageId))

        googleMap.clear()
        googleMap.addMarker(marker).showInfoWindow()
    }

    private fun checkPermission(): Boolean {
        return UIHelper.requestPermission(
            activity,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Constant.REQUEST_CODE_LOCATION_PERMISSION
        ) && UIHelper.requestPermission(
            activity,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Constant.REQUEST_CODE_LOCATION_PERMISSION
        )
    }

    private fun requestCurrentLocation() {
        if (!checkPermission()) {
            return
        }

        googleApiClient.let {
            if (googleApiClient.isConnected) {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, locationListener)
            }
        }
    }

    fun setMapsManagerListener(mapsManagerListener: MapsManagerListener) {
        this.mapsManagerListener = mapsManagerListener
    }
}