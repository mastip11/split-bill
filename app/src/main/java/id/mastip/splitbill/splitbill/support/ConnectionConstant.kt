package id.mastip.splitbill.splitbill.support

object ConnectionConstant {
    const val BASE_URL = "http://192.168.0.105:3000/"

    const val LOGIN = BASE_URL + "login"
    const val GET_HISTORY = BASE_URL + "history/get"
    const val GET_ORDER = BASE_URL + "order/get"
    const val UPDATE_ORDER_ITEM = BASE_URL + "order/update_item"
}