package id.mastip.splitbill.splitbill.manager.connection

import android.content.Context
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.connection.listener.BasicListener
import id.mastip.splitbill.splitbill.manager.connection.listener.ConnectionListener
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.model.BaseResponseModel
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.support.ConnectionConstant
import id.mastip.splitbill.splitbill.support.Constant
import org.json.JSONObject
import java.text.SimpleDateFormat
import id.mastip.splitbill.splitbill.support.UIHelper
import org.json.JSONArray

object TransactionConnectionManager: BaseConnectionManager() {

    fun getSingleton(context: Context): TransactionConnectionManager {
        parentSingleton(context)
        return this
    }

    fun getAllHistory(id: Int, listener: DataReturnListener<MutableList<TransactionModel>>) {
        val json = JSONObject()
        json.put(Constant.KEY_ID, id)
        requestPOST(ConnectionConstant.GET_HISTORY, json, object : BasicListener {

            val transactionList: MutableList<TransactionModel> = ArrayList()

            override fun onResponse(model: BaseResponseModel) {
                val jsonArray = JSONArray(model.responseValue)
                for (a in 0 until jsonArray.length()) {
                    val objectTransaction = jsonArray.getJSONObject(a)

                    val transactionModel = TransactionModel()
                    transactionModel.id = objectTransaction.getInt(Constant.KEY_ID)
                    transactionModel.restaurantName = objectTransaction.getString(Constant.KEY_MERCHANT_NAME)
                    transactionModel.date = UIHelper.convertDateToLong(objectTransaction.getString(Constant.KEY_CREATED_AT))
                    transactionModel.id = objectTransaction.getInt(Constant.KEY_ID)

                    transactionList.add(transactionModel)
                }

                listener.onSuccess(model.message, transactionList)
            }

            override fun onFailed(errorCode: Int, message: String) {
                listener.onFailed(message)
            }
        })
    }
}