package id.mastip.splitbill.splitbill.manager.connection

import android.content.Context
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.connection.listener.BasicListener
import id.mastip.splitbill.splitbill.manager.connection.listener.ConnectionListener
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.model.BaseResponseModel
import id.mastip.splitbill.splitbill.support.ConnectionConstant
import id.mastip.splitbill.splitbill.support.Constant
import org.json.JSONArray
import org.json.JSONObject

object UserConnectionManager: BaseConnectionManager() {

    fun getSingleton(context: Context): UserConnectionManager {
        parentSingleton(context)
        return this
    }

    fun login(name: String, email: String, photoUrl: String, googleId: String, fcmId: String, listener: DataReturnListener<Int>) {
        val json = JSONObject()
        json.put(Constant.KEY_GOOGLE_ID, googleId)
        json.put(Constant.KEY_NAME, name)
        json.put(Constant.KEY_EMAIL, email)
        json.put(Constant.KEY_PHOTO_URL, photoUrl)
        json.put(Constant.KEY_FCM_ID, fcmId)
        requestPOST(ConnectionConstant.LOGIN, json, object : BasicListener {

            var id: Int = 0
            override fun onResponse(model: BaseResponseModel) {
                val jsonArray = JSONArray(model.responseValue)

                if (jsonArray.length() > 0) {
                    val jsonObject = jsonArray.getJSONObject(0)

                    if (jsonObject.has(Constant.KEY_ID)) {
                        id = jsonObject.getInt(Constant.KEY_ID)
                    }
                }
                listener.onSuccess(model.message, id)
            }

            override fun onFailed(errorCode: Int, message: String) {
                listener.onFailed(message)
            }
        })
    }
}