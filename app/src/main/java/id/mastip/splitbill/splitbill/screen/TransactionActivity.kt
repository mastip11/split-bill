package id.mastip.splitbill.splitbill.screen

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import butterknife.BindView
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.presenter.TransactionPresenter
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.TransactionListAdapter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivity
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivityWithoutInjection
import javax.inject.Inject


class TransactionActivity : CustomBaseActivityWithoutInjection() {

    @BindView(R.id.transaction_list)
    protected lateinit var listTransaction: RecyclerView

    protected lateinit var presenter: TransactionPresenter

    lateinit var adapter: TransactionListAdapter

    private val listener = object : OnItemClickedListener<TransactionModel>{
        override fun onItemClickedListener(model: TransactionModel) {
            //to do write here
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_transaction
    }

    override fun initializeDefaultValue() {
        presenter = TransactionPresenter(this)
        presenter.setUpAdapter()
        setUpRecyclerView()
    }

    fun setAdapter(list: List<TransactionModel>){
        adapter = TransactionListAdapter(list, listener)
    }

    private fun setUpRecyclerView(){
        listTransaction.itemAnimator = DefaultItemAnimator();
        listTransaction.layoutManager = LinearLayoutManager(this);
        listTransaction.adapter = adapter
    }
}
