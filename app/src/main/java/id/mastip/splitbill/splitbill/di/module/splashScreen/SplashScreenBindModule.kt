package id.mastip.splitbill.splitbill.di.module.splashScreen

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.mastip.splitbill.splitbill.screen.SplashScreenActivity

@Module
abstract class SplashScreenBindModule {

    @ContributesAndroidInjector(modules = [SplashScreenModule::class])
    abstract fun contributeSplashScreenActivity(): SplashScreenActivity
}