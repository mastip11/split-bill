package id.mastip.splitbill.splitbill.support.adapter.recyclerView

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder.AdditionalTaxViewHolder
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder.OrderViewHolder

class OrderAdapter(private val list: List<OrderModel>, private val listener: OnItemClickedListener<OrderModel>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    private val TYPE_ORDER = 1
    private val TYPE_ADDITIONAL = 2

    override fun getItemViewType(position: Int): Int {
        return if (position != list.size) {
            TYPE_ORDER
        } else {
            TYPE_ADDITIONAL
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
        return if (p1 == TYPE_ORDER) {
            val view = LayoutInflater.from(p0.context).inflate(R.layout.custom_transaction, p0, false)
            OrderViewHolder(view, listener)
        } else {
            val view = LayoutInflater.from(p0.context).inflate(R.layout.view_holder_additional_tax, p0, false)
            AdditionalTaxViewHolder(view)
        }
    }

    override fun getItemCount(): Int = list.size + 1

    override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
        if (p1 != list.size) {
            (p0 as OrderViewHolder).setUpToUI(list[p1])
        }
    }
}
