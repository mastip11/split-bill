package id.mastip.splitbill.splitbill.support.dialog

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import butterknife.ButterKnife
import butterknife.OnClick
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.support.dialog.listener.CameraPickerListener

class CameraPickerDialog(context: Context): AlertDialog(context) {

    lateinit var cameraListener: CameraPickerListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_camera_picker)
        ButterKnife.bind(this)
    }

    @OnClick(R.id.scanBillTextView)
    protected fun onBillClicked() {
        cameraListener.onScanBillClicked()
    }

    @OnClick(R.id.scanQRCodeTextView)
    protected fun onQRClicked() {
        cameraListener.onScanQRClicked()
    }
}