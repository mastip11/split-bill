package id.mastip.splitbill.splitbill.screen

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.navigator.Navigate
import id.mastip.splitbill.splitbill.model.MenuModel
import id.mastip.splitbill.splitbill.screen.presenter.HomePresenter
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.MenuAdapter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivity
import id.mastip.splitbill.splitbill.support.dialog.CameraPickerDialog
import id.mastip.splitbill.splitbill.support.dialog.listener.CameraPickerListener
import javax.inject.Inject

class HomeActivity : CustomBaseActivity() {

    @BindView(R.id.homeRecyclerView)
    protected lateinit var listMenuRecyclerView: RecyclerView

    @BindView(R.id.greetingTextView)
    protected lateinit var greetingTextView: TextView

    @BindView(R.id.advertisementImageView)
    protected lateinit var advertisementImageView: ImageView

    @BindView(R.id.profileImageView)
    protected lateinit var profileImageView: CircleImageView

    @Inject
    protected lateinit var presenter: HomePresenter

    @Inject
    protected lateinit var navigate: Navigate

    private lateinit var adapter: MenuAdapter

    private val listener = object : OnItemClickedListener<MenuModel> {
        override fun onItemClickedListener(model: MenuModel) {
            presenter.onItemClicked(model)
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_home

    override fun initializeDefaultValue() {
        presenter.setUpAdapter()
        setUpRecyclerView()
        setUpUI()
    }

    fun setUpAdapter(list: List<MenuModel>) {
        adapter = MenuAdapter(list, listener)
    }

    fun goToScanActivity() {
        val dialog = CameraPickerDialog(this)
        dialog.cameraListener = object : CameraPickerListener {
            override fun onScanBillClicked() {
                startActivity(navigate.toScanActivity(this@HomeActivity))
            }

            override fun onScanQRClicked() {
                startActivity(navigate.toScanQRActivity(this@HomeActivity))
            }
        }
        dialog.show()
    }

    fun goToHistory() {
        startActivity(navigate.toHistoryActivity(this))
    }

    fun goToVoucher() {
        startActivity(navigate.toVoucherActivity(this))
    }

    fun goToMerchant() {
        startActivity(navigate.toMerchantActivity(this))
    }

    fun logout() {
        DataCenterManager.getSingleton(this).clearAllData()
        finish()
        startActivity(navigate.toLoginActivity(this))
    }

    private fun setUpUI() {
        greetingTextView.text = "Hello, " + DataCenterManager.getSingleton(this).name

        Picasso.get().load("https://www.syfy.com/sites/syfy/files/styles/1200x1200/public/2017/11/thor_the_dark_world_01.jpg?itok=55DZmZyS&timestamp=1509647127").into(profileImageView)
        Picasso.get().load("https://www.giladiskon.com/images/deals/photo%20-%201514421841.jpg").into(advertisementImageView)
    }

    private fun setUpRecyclerView() {
        listMenuRecyclerView.itemAnimator = DefaultItemAnimator()
        listMenuRecyclerView.layoutManager = GridLayoutManager(this, 2)
        listMenuRecyclerView.adapter = adapter
    }
}