package id.mastip.splitbill.splitbill.screen

import android.content.Intent
import butterknife.BindView
import butterknife.OnClick
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.SignInButton
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.manager.google.SignInManager
import id.mastip.splitbill.splitbill.manager.navigator.Navigate
import id.mastip.splitbill.splitbill.screen.presenter.LoginPresenter
import id.mastip.splitbill.splitbill.support.Constant
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivity
import javax.inject.Inject

class LoginActivity : CustomBaseActivity() {

    @BindView(R.id.loginSignInButton)
    lateinit var signInButton: SignInButton

    @Inject
    lateinit var navigate: Navigate

    lateinit var presenter: LoginPresenter

    @Inject
    lateinit var signInManager: SignInManager

    override fun getLayoutId(): Int = R.layout.activity_login

    override fun initializeDefaultValue() {
        signInButton.setSize(SignInButton.SIZE_WIDE)
        presenter = LoginPresenter(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constant.REQUEST_CODE_GOOGLE_SIGN_IN) {
            val googleSignInResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data)

            if (googleSignInResult.isSuccess) {
                presenter.handleGoogleSignIn(googleSignInResult.signInAccount)
            }
        }
    }

    @OnClick(R.id.loginSignInButton)
    fun onGoogleSignInClicked() {
        val intent = signInManager.googleSignInClient.signInIntent
        startActivityForResult(intent, Constant.REQUEST_CODE_GOOGLE_SIGN_IN)
    }

    fun goToHomeScreen() {
        startActivity(navigate.toHomeActivity(this))
    }
}