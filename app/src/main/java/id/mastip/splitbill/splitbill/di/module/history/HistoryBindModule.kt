package id.mastip.splitbill.splitbill.di.module.history

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.mastip.splitbill.splitbill.screen.HistoryActivity

@Module
abstract class HistoryBindModule {

    @ContributesAndroidInjector(modules = [HistoryModule::class])
    abstract fun contributeHistoryActivity(): HistoryActivity
}