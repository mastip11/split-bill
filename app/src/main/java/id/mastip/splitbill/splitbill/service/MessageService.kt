package id.mastip.splitbill.splitbill.service

import android.content.Intent
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.support.Constant

class MessageService: FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage?) {
        p0?.let {
            sendBroadcastReceiver(it.data)
        }
    }

    private fun sendBroadcastReceiver(data: MutableMap<String, String>) {
        val model = OrderModel()

        data[Constant.KEY_ID]?.let {
            model.id = it.toInt()
        }

        data[Constant.KEY_PRICE]?.let {
            model.price = it.toInt()
        }

        data[Constant.KEY_IS_MY_TAG]?.let {
            model.isMyTag = it.toBoolean()
        }

        data[Constant.KEY_NAME]?.let {
            model.nama = it
        }

        val intent = Intent(Constant.ACTION_UPDATE_ORDER)
        intent.putExtra(Constant.KEY_DATA, model)
        sendBroadcast(intent)
    }
}