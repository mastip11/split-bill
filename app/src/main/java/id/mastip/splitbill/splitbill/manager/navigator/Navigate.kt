package id.mastip.splitbill.splitbill.manager.navigator

import android.content.Context
import android.content.Intent
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.*
import id.mastip.splitbill.splitbill.support.Constant

class Navigate {

    fun toLoginActivity(context: Context): Intent {
        return Intent(context, LoginActivity::class.java)
    }

    fun toHomeActivity(context: Context): Intent {
        return Intent(context, HomeActivity::class.java)
    }

    fun toScanActivity(context: Context): Intent {
        return Intent(context, ScanActivity::class.java)
    }

    fun toScanQRActivity(context: Context): Intent {
        return Intent(context, BarcodeCaptureActivity::class.java)
    }

    fun toHistoryActivity(context: Context): Intent {
        return Intent(context, HistoryActivity::class.java)
    }

    fun toDetailTransactionActivity(context: Context, model: TransactionModel): Intent {
        val intent = Intent(context, DetailTransactionActivity::class.java)
        intent.putExtra(Constant.KEY_DATA, model)
        return intent
    }

    fun toVoucherActivity(context: Context): Intent {
        return Intent(context, VoucherActivity::class.java)
    }

    fun toMerchantActivity(context: Context): Intent {
        return Intent(context, MerchantActivity::class.java)
    }
}