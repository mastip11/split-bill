package id.mastip.splitbill.splitbill.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransactionModel(
    var id: Int? = 0,
    var restaurantName: String? = "",
    var date: Long? = 0,
    var restaurantImage: String? = "",
    var amount: Int? = 0): Parcelable

