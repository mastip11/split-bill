package id.mastip.splitbill.splitbill.support.adapter.listener

interface OnItemClickedListener<T> {

    fun onItemClickedListener(model: T)
}