package id.mastip.splitbill.splitbill.screen.presenter

import android.widget.Toast
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.connection.TransactionConnectionManager
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.HistoryActivity

class HistoryPresenter(private val activity: HistoryActivity) {

    private var transactionModels = ArrayList<TransactionModel>()

    fun setUpAdapter() {
        activity.setUpAdapter(transactionModels)
    }

    fun requestDataToServer() {
        activity.showLoading()
        TransactionConnectionManager.getSingleton(activity).getAllHistory(DataCenterManager.getSingleton(activity).id, object : DataReturnListener<MutableList<TransactionModel>> {
            override fun onSuccess(message: String, data: MutableList<TransactionModel>) {
                activity.dismissLoading()
                this@HistoryPresenter.transactionModels.clear()
                this@HistoryPresenter.transactionModels.addAll(data)
                activity.adapter.notifyDataSetChanged()

                activity.validateUI(this@HistoryPresenter.transactionModels.size)
            }

            override fun onFailed(message: String) {
                activity.dismissLoading()
                Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}