package id.mastip.splitbill.splitbill.di.module.login

import dagger.Module
import dagger.Provides
import id.mastip.splitbill.splitbill.manager.google.SignInManager
import id.mastip.splitbill.splitbill.screen.LoginActivity
import id.mastip.splitbill.splitbill.screen.presenter.LoginPresenter

@Module
class LoginModule {

    @Provides
    fun providesLoginPresenter(loginActivity: LoginActivity): LoginPresenter {
        return LoginPresenter(loginActivity)
    }

    @Provides
    fun provideSignInManager(loginActivity: LoginActivity): SignInManager {
        return SignInManager(loginActivity)
    }
}