package id.mastip.splitbill.splitbill.support

import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.MenuModel
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.model.VoucherModel
import java.util.*
import kotlin.collections.ArrayList

class Generator {

    companion object {

        fun generateMenuModels(): List<MenuModel> {
            val list: ArrayList<MenuModel> = ArrayList()
            list.add(MenuModel(1, "Scan", R.drawable.ic_scan))
            list.add(MenuModel(2, "History", R.drawable.ic_history))
//            list.add(MenuModel(4, "Voucher", R.drawable.ic_voucher))
            list.add(MenuModel(5, "Merchant", R.drawable.ic_merchant))
            list.add(MenuModel(3, "Logout", R.drawable.ic_logout))
            return list
        }

        fun generateTransaction(): ArrayList<TransactionModel>{
            val list: ArrayList<TransactionModel> = ArrayList()
            list.add(TransactionModel(1, "Pizza Hut", Date().time, "https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/11/02/104815390-pizza-hut.530x298.jpg?v=1509630852", 10000))
            list.add(TransactionModel(2, "Pizza Hut", Date().time, "https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/11/02/104815390-pizza-hut.530x298.jpg?v=1509630852", 10000))
            list.add(TransactionModel(3, "Mujigae", Date().time, "https://fm.cnbc.com/applications/cnbc.com/resources/img/editorial/2017/11/02/104815390-pizza-hut.530x298.jpg?v=1509630852", 20000))
            return list
        }

        fun generateOrder(): List<OrderModel>{
            val list: ArrayList<OrderModel> = ArrayList()
            list.add(OrderModel(1, 2, "Nasi Goreng", 20000))
            list.add(OrderModel(1, 2, "Nasi Ayam", 10000))
            list.add(OrderModel(1, 2, "Nasi Indomie", 15000))
            return list
        }

        fun generateVoucher(): ArrayList<VoucherModel> {
            val list = ArrayList<VoucherModel>()
            list.add(VoucherModel(1, "Voucher KFC", "", 10000))
            list.add(VoucherModel(2, "Voucher Mujigae", "", 100000))
            list.add(VoucherModel(3, "Voucher MCD", "", 10000))
            return list
        }
    }
}