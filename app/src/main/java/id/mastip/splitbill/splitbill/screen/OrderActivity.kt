package id.mastip.splitbill.splitbill.screen

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import butterknife.BindView
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.screen.presenter.OrderPresenter
import id.mastip.splitbill.splitbill.support.Constant
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.OrderAdapter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivityWithoutInjection

class OrderActivity: CustomBaseActivityWithoutInjection() {

    @BindView(R.id.transaction_list)
    protected lateinit var listOrder: RecyclerView

    protected lateinit var presenter: OrderPresenter

    lateinit var adapter: OrderAdapter

    private val listener = object : OnItemClickedListener<OrderModel> {
        override fun onItemClickedListener(model: OrderModel) {
            //to do write here
        }
    }

    private val broadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            presenter.onReceiveUpdateTag(context, intent)
        }
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_transaction
    }

    override fun initializeDefaultValue() {
        presenter = OrderPresenter(this)
        presenter.setUpAdapter()
    }

    override fun onResume() {
        super.onResume()
        val intentFilter = IntentFilter(Constant.ACTION_UPDATE_ORDER)
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
    }

    fun setAdapter(list: List<OrderModel>){
        adapter = OrderAdapter(list, listener)
        listOrder.itemAnimator = DefaultItemAnimator()
        listOrder.layoutManager = LinearLayoutManager(this)
        listOrder.adapter = adapter
    }
}
