package id.mastip.splitbill.splitbill.screen.presenter

import android.widget.Toast
import id.mastip.splitbill.splitbill.manager.DataCenterManager
import id.mastip.splitbill.splitbill.manager.connection.TransactionConnectionManager
import id.mastip.splitbill.splitbill.manager.connection.listener.ConnectionListener
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.TransactionActivity
import id.mastip.splitbill.splitbill.support.Generator

class TransactionPresenter(private var activity: TransactionActivity){

    private var list: MutableList<TransactionModel> = Generator.generateTransaction()

    fun setUpAdapter(){
        activity.setAdapter(list)
    }
}
