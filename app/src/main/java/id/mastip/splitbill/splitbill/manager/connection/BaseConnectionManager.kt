package id.mastip.splitbill.splitbill.manager.connection

import android.content.Context
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import id.mastip.splitbill.splitbill.manager.connection.listener.BasicListener
import id.mastip.splitbill.splitbill.model.BaseResponseModel
import id.mastip.splitbill.splitbill.support.Constant
import org.json.JSONException
import org.json.JSONObject
import java.util.HashMap

open class BaseConnectionManager {

    private var requestQueue: RequestQueue? = null

    protected fun parentSingleton(context: Context): BaseConnectionManager {
        requestQueue = Volley.newRequestQueue(context)
        return this
    }

    fun requestPOST(url: String, params: JSONObject, listener: BasicListener? = null) {
        val stringRequest = object : JsonObjectRequest(Request.Method.POST, url, params, Response.Listener { response ->
            try {
                listener?.onResponse(parseResponseModel(response))
            } catch (e: JSONException) {
                listener?.onFailed(Constant.CODE_ERROR_PARSE, "Something failed")
            }
        }, Response.ErrorListener { error ->
            var errorMessage = "Undefined Error"
            var errorCode = Constant.CODE_ERROR_UNDEFINED
            if (error is TimeoutError || error is NoConnectionError) {
                errorMessage = "Please turn on your connectivity"
                errorCode = Constant.CODE_ERROR_CONNECTION
            } else if (error is AuthFailureError) {
                errorMessage = "Authentication Error"
                errorCode = Constant.CODE_ERROR_UNAUTHENTICATE
            } else if (error is ServerError) {
                errorMessage = "Server Error"
                errorCode = Constant.CODE_ERROR_SERVER
            } else if (error is NetworkError) {
                errorMessage = "Network Error"
                errorCode = Constant.CODE_ERROR_NETWORK
            } else if (error is ParseError) {
                errorMessage = "Parse Error"
                errorCode = Constant.CODE_ERROR_PARSE_SERVER
            }
            listener?.onFailed(errorCode, errorMessage)
        }) {

        }
        stringRequest.retryPolicy = DefaultRetryPolicy(
            30000,
            0,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        requestQueue?.add(stringRequest)
    }

    fun requestGET(url: String, listener: BasicListener? = null) {
        val stringRequest = object : StringRequest(Request.Method.GET, url, Response.Listener { response ->
            try {
                val jsonObject = JSONObject(response)
                listener?.onResponse(parseResponseModel(jsonObject))
            } catch (e: JSONException) {
                listener?.onFailed(Constant.CODE_ERROR_PARSE, "Something failed")
            }
        }, Response.ErrorListener { error ->
            var errorMessage = "Undefined Error"
            var errorCode = Constant.CODE_ERROR_UNDEFINED
            if (error is TimeoutError || error is NoConnectionError) {
                errorMessage = "Please turn on your connectivity"
                errorCode = Constant.CODE_ERROR_CONNECTION
            } else if (error is AuthFailureError) {
                errorMessage = "Authentication Error"
                errorCode = Constant.CODE_ERROR_UNAUTHENTICATE
            } else if (error is ServerError) {
                errorMessage = "Server Error"
                errorCode = Constant.CODE_ERROR_SERVER
            } else if (error is NetworkError) {
                errorMessage = "Network Error"
                errorCode = Constant.CODE_ERROR_NETWORK
            } else if (error is ParseError) {
                errorMessage = "Parse Error"
                errorCode = Constant.CODE_ERROR_PARSE_SERVER
            }
            listener?.onFailed(errorCode, errorMessage)
        }) {

        }
        stringRequest.retryPolicy = DefaultRetryPolicy(
            30000,
            0,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)
        requestQueue?.add(stringRequest)
    }

    private fun parseResponseModel(jsonObject: JSONObject): BaseResponseModel {
        val baseResponseModel = BaseResponseModel()
        try {
            if (jsonObject.has(Constant.KEY_RESULT_CODE)) {
                baseResponseModel.responseCode = jsonObject.getInt(Constant.KEY_RESULT_CODE)
            }
            if (jsonObject.has(Constant.KEY_MESSAGE)) {
                baseResponseModel.message = jsonObject.getString(Constant.KEY_MESSAGE)
            }
            if (jsonObject.has(Constant.KEY_VALUES)) {
                baseResponseModel.responseValue = jsonObject.getString(Constant.KEY_VALUES)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

        return baseResponseModel
    }
}