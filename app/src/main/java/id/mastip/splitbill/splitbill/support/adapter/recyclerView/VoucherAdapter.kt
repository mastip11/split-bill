package id.mastip.splitbill.splitbill.support.adapter.recyclerView

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.VoucherModel
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder.VoucherViewHolder

class VoucherAdapter(var list: ArrayList<VoucherModel>): RecyclerView.Adapter<VoucherViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VoucherViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.view_holder_voucher, p0, false)
        return VoucherViewHolder(view)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: VoucherViewHolder, p1: Int) {
        p0.setUpModelToUI(list[p1])
    }

}