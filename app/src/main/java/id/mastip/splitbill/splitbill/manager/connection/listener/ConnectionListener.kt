package id.mastip.splitbill.splitbill.manager.connection.listener

interface ConnectionListener {
    fun onSuccess(message: String)
    fun onFailed(message: String)
}