package id.mastip.splitbill.splitbill.manager.google

import android.content.Context
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions

class SignInManager(context: Context) {

    private var googleSignInOptions: GoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
    var googleSignInClient: GoogleSignInClient

    init {
        googleSignInClient = GoogleSignIn.getClient(context, googleSignInOptions)
    }
}