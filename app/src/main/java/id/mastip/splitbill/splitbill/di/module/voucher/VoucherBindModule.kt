package id.mastip.splitbill.splitbill.di.module.voucher

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.mastip.splitbill.splitbill.screen.VoucherActivity

@Module
abstract class VoucherBindModule {

    @ContributesAndroidInjector(modules = [VoucherModule::class])
    abstract fun contributeVoucherActivity(): VoucherActivity
}