package id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import java.text.SimpleDateFormat
import java.util.*

class TransactionViewHolder(view: View, private val listener: OnItemClickedListener<TransactionModel>) : RecyclerView.ViewHolder(view) {

    @BindView(R.id.custom_transaction_name)
    protected lateinit var tvName: TextView

    @BindView(R.id.custom_transaction_date)
    protected lateinit var tvDate: TextView

    @BindView(R.id.custom_transaction_image)
    protected lateinit var ivImage: ImageView

    @BindView(R.id.custom_transaction_amount)
    protected lateinit var amountTextView: TextView

    private lateinit var data: TransactionModel

    private var dateFormat = SimpleDateFormat("dd MMMM yyyy", Locale.US)

    init {
        ButterKnife.bind(this, view)
    }

    fun setUpToUI(data: TransactionModel) {
        this.data = data
        tvName.text = data.restaurantName
        tvDate.text = dateFormat.format(data.date)
        amountTextView.text = "Rp. ${data.amount}"
    }

    @OnClick
    protected fun onBaseClicked(){
        listener.onItemClickedListener(data)
    }
}
