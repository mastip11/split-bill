package id.mastip.splitbill.splitbill.screen

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.manager.navigator.Navigate
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.presenter.HistoryPresenter
import id.mastip.splitbill.splitbill.support.Generator
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.TransactionListAdapter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivity
import javax.inject.Inject

class HistoryActivity : CustomBaseActivity() {

    @BindView(R.id.recyclerViewTransaction)
    protected lateinit var transactionList: RecyclerView

    @BindView(R.id.noDataTextView)
    protected lateinit var noDataTextView: TextView

    @Inject
    lateinit var presenter: HistoryPresenter

    @Inject
    lateinit var navigate: Navigate

    lateinit var adapter: TransactionListAdapter

    private var listener = object : OnItemClickedListener<TransactionModel> {
        override fun onItemClickedListener(model: TransactionModel) {
            startActivity(navigate.toDetailTransactionActivity(this@HistoryActivity, model))
        }
    }

    override fun getLayoutId(): Int = R.layout.activity_history

    override fun initializeDefaultValue() {
        super.initializeDefaultValue()
        presenter.setUpAdapter()
        presenter.requestDataToServer()
    }

    fun setUpAdapter(transactionModels: MutableList<TransactionModel>) {
        adapter = TransactionListAdapter(transactionModels, listener)
        transactionList.layoutManager = LinearLayoutManager(this)
        transactionList.itemAnimator = DefaultItemAnimator()
        transactionList.adapter = adapter
    }

    fun validateUI(size: Int) {
        if (size == 0) {
            noDataTextView.visibility = View.VISIBLE
        } else {
            noDataTextView.visibility = View.GONE
        }
    }

    @OnClick(R.id.backButton)
    protected fun onBackButtonClicked() {
        finish()
    }
}