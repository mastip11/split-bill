package id.mastip.splitbill.splitbill.screen.presenter

import android.content.Context
import android.content.Intent
import android.widget.Toast
import id.mastip.splitbill.splitbill.manager.connection.OrderConnectionManager
import id.mastip.splitbill.splitbill.manager.connection.listener.DataReturnListener
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.OrderActivity
import id.mastip.splitbill.splitbill.support.Constant
import id.mastip.splitbill.splitbill.support.Generator

class OrderPresenter(private var activity: OrderActivity) {

    private var list: MutableList<OrderModel> = ArrayList()
    private var transactionModel = TransactionModel()

    init {
        getDatafromInten()
    }

    fun setUpAdapter() {
        activity.setAdapter(list)
    }

    fun getDatafromInten() {
        if (activity.intent.hasExtra(Constant.KEY_DATA)) {
            transactionModel = activity.intent.getParcelableExtra(Constant.KEY_DATA)
        }
    }

    fun onReceiveUpdateTag(context: Context?, intent: Intent?) {
        intent?.let {
            if (it.hasExtra(Constant.KEY_DATA)) {
                val model = it.getParcelableExtra<OrderModel>(Constant.KEY_DATA)

                for (orderModel in list) {
                    if (orderModel.id == model.id) {
                        list[list.indexOf(orderModel)] = model
                        break
                    }
                }

                activity.adapter.notifyDataSetChanged()
            }
        }
    }
}
