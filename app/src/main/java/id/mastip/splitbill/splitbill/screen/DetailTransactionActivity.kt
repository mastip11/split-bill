package id.mastip.splitbill.splitbill.screen

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.OnClick
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.google.zxing.common.BitMatrix
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.squareup.picasso.Picasso
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.model.OrderModel
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.screen.presenter.DetailTransactionPresenter
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.OrderAdapter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivityWithoutInjection
import id.mastip.splitbill.splitbill.support.dialog.BarcodeDialog
import java.text.SimpleDateFormat
import java.util.*

class DetailTransactionActivity: CustomBaseActivityWithoutInjection() {

    @BindView(R.id.toolbarTitleTextView)
    protected lateinit var titleTextView: TextView

    @BindView(R.id.restaurantImageView)
    protected lateinit var restaurantImageView: ImageView

    @BindView(R.id.restaurantNameTextView)
    protected lateinit var restaurantNameTextView: TextView

    @BindView(R.id.totalBillTextView)
    protected lateinit var totalBillTextView: TextView

    @BindView(R.id.transactionDateTextView)
    protected lateinit var dateTextView: TextView

    @BindView(R.id.listRecyclerView)
    protected lateinit var listRecyclerView: RecyclerView

    @BindView(R.id.barcodeImageView)
    protected lateinit var barcodeImageView: ImageView

    protected lateinit var presenter: DetailTransactionPresenter

    lateinit var adapter: OrderAdapter

    private val listener = object : OnItemClickedListener<OrderModel> {
        override fun onItemClickedListener(model: OrderModel) {
            presenter.onItemClickedListener(model)
        }
    }

    override fun getLayoutId(): Int = R.layout.detail_transaction_activity

    override fun initializeDefaultValue() {
        super.initializeDefaultValue()
        presenter = DetailTransactionPresenter(this)
    }

    @OnClick(R.id.backButton)
    internal fun backClicked() {
        onBackPressed()
    }

    @OnClick(R.id.barcodeImageView)
    internal fun showBarcodeClicked() {
        presenter.showBarcodeClicked()
    }

    fun showBarcodeDialog(id: String) {
        val barcodeDialog = BarcodeDialog(this, id)
        barcodeDialog.show()
    }

    fun setUpUI(model: TransactionModel) {
        titleTextView.text = model.restaurantName
        model.restaurantImage?.let {
            if (it.isNotEmpty()) {
                Picasso.get().load(model.restaurantImage).into(restaurantImageView)
            }
        }
        restaurantNameTextView.text = model.restaurantName
        totalBillTextView.text = "Rp. ${model.amount}"
        dateTextView.text = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()).format(model.date)
    }

    fun setAdapter(list: List<OrderModel>) {
        adapter = OrderAdapter(list, listener)
        listRecyclerView.itemAnimator = DefaultItemAnimator()
        listRecyclerView.layoutManager = LinearLayoutManager(this)
        listRecyclerView.adapter = adapter
    }
}