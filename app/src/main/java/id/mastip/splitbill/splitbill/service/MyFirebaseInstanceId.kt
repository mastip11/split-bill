package id.mastip.splitbill.splitbill.service

import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdService

class MyFirebaseInstanceId: FirebaseInstanceIdService() {

    override fun onTokenRefresh() {
        sendFirebaseInstanceIdToServer(FirebaseInstanceId.getInstance().token)
    }

    private fun sendFirebaseInstanceIdToServer(id: String?) {

    }
}