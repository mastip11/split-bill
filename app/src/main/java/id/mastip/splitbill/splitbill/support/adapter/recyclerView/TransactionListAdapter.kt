package id.mastip.splitbill.splitbill.support.adapter.recyclerView

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import id.mastip.splitbill.splitbill.model.TransactionModel
import id.mastip.splitbill.splitbill.support.adapter.listener.OnItemClickedListener
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.viewHolder.TransactionViewHolder

class TransactionListAdapter(private val list: List<TransactionModel>, private val listener: OnItemClickedListener<TransactionModel>) : RecyclerView.Adapter<TransactionViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TransactionViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(id.mastip.splitbill.splitbill.R.layout.custom_transaction, p0, false)
        return TransactionViewHolder(view, listener)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(p0: TransactionViewHolder, p1: Int) {
        p0.setUpToUI(list[p1])
    }
}
