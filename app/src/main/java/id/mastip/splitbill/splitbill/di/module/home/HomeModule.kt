package id.mastip.splitbill.splitbill.di.module.home

import dagger.Module
import dagger.Provides
import id.mastip.splitbill.splitbill.screen.HomeActivity
import id.mastip.splitbill.splitbill.screen.presenter.HomePresenter

@Module
class HomeModule {

    @Provides
    fun provideHomePresenter(homeActivity: HomeActivity): HomePresenter {
        return HomePresenter(homeActivity)
    }
}