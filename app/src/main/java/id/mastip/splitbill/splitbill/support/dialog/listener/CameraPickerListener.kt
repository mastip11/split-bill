package id.mastip.splitbill.splitbill.support.dialog.listener

interface CameraPickerListener {
    fun onScanBillClicked()
    fun onScanQRClicked()
}