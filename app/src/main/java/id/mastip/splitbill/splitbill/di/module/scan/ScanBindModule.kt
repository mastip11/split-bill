package id.mastip.splitbill.splitbill.di.module.scan

import dagger.Module
import dagger.android.ContributesAndroidInjector
import id.mastip.splitbill.splitbill.screen.ScanActivity

@Module
abstract class ScanBindModule {

    @ContributesAndroidInjector(modules = [ScanModule::class])
    abstract fun contributeScanActivity(): ScanActivity
}