package id.mastip.splitbill.splitbill.screen.presenter

import id.mastip.splitbill.splitbill.model.MerchantModel
import id.mastip.splitbill.splitbill.screen.MerchantActivity

class MerchantPresenter(activity: MerchantActivity) {

    private val listMerchant: MutableList<MerchantModel> = ArrayList()

    init {
        activity.configureRecyclerView(listMerchant)
        dataDummy()
        activity.adapter.notifyDataSetChanged()
    }

    private fun dataDummy() {
        val merchantModel = MerchantModel()
        merchantModel.id = 1
        merchantModel.name = "KFC Margonda"
        merchantModel.description = "Jaringan resto cepat saji, terkenal dengan ayam goreng, sayap ayam & menu pendamping."
        merchantModel.rating = 4.5
        merchantModel.address = "Jalan Margonda Raya No.88, Kemiri Muka, Beji, Kemiri Muka, Beji, Kota Depok, Jawa Barat 16424"
        merchantModel.iconURL = "http://www.stickpng.com/assets/images/590607570cbeef0acff9a641.png"
        merchantModel.latitude = 106.0
        merchantModel.longitude = 18.0

        listMerchant.add(merchantModel)

        val merchantModel1 = MerchantModel()
        merchantModel1.id = 1
        merchantModel1.name = "KFC Margonda"
        merchantModel1.description = "Jaringan resto cepat saji, terkenal dengan ayam goreng, sayap ayam & menu pendamping."
        merchantModel1.rating = 4.5
        merchantModel1.address = "Jalan Margonda Raya No.88, Kemiri Muka, Beji, Kemiri Muka, Beji, Kota Depok, Jawa Barat 16424"
        merchantModel1.iconURL = "http://www.stickpng.com/assets/images/590607570cbeef0acff9a641.png"
        merchantModel1.latitude = 106.0
        merchantModel1.longitude = 18.0

        listMerchant.add(merchantModel1)
    }
}