package id.mastip.splitbill.splitbill.screen

import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import butterknife.BindView
import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.support.Generator
import id.mastip.splitbill.splitbill.support.adapter.recyclerView.VoucherAdapter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivity

class VoucherActivity: CustomBaseActivity() {

    @BindView(R.id.listVoucher)
    protected lateinit var voucherRecyclerView: RecyclerView

    var listVoucher = Generator.generateVoucher()

    private lateinit var adapter: VoucherAdapter

    override fun getLayoutId(): Int = R.layout.activity_voucher

    override fun initializeDefaultValue() {
        super.initializeDefaultValue()
        setUpAdapter()
    }

    fun setUpAdapter() {
        adapter = VoucherAdapter(listVoucher)
        voucherRecyclerView.itemAnimator = DefaultItemAnimator()
        voucherRecyclerView.layoutManager = LinearLayoutManager(this)
        voucherRecyclerView.adapter = adapter
    }

}