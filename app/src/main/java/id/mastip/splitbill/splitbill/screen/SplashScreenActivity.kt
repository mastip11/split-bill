package id.mastip.splitbill.splitbill.screen

import id.mastip.splitbill.splitbill.R
import id.mastip.splitbill.splitbill.manager.navigator.Navigate
import id.mastip.splitbill.splitbill.screen.presenter.SplashScreenPresenter
import id.mastip.splitbill.splitbill.support.base.CustomBaseActivity
import javax.inject.Inject

class SplashScreenActivity : CustomBaseActivity() {

    @Inject
    lateinit var navigate: Navigate

    @Inject
    lateinit var presenter: SplashScreenPresenter

    override fun getLayoutId(): Int = R.layout.activity_splash_screen

    fun moveToLogin() {
        startActivity(navigate.toLoginActivity(this))
    }

}