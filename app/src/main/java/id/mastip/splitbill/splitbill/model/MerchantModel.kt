package id.mastip.splitbill.splitbill.model

data class MerchantModel(
    var id: Int? = 0,
    var name: String? = "",
    var rating: Double? = 0.0,
    var address: String? = "",
    var description: String? = "",
    var latitude: Double? = 0.0,
    var longitude: Double? = 0.0,
    var iconURL: String? = ""
)