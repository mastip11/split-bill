package id.mastip.splitbill.splitbill.support

import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

object UIHelper {
    fun requestPermission(activity: Activity, permission: String, requestCode: Int): Boolean {
        var isGranted = true
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(activity, permission) != PackageManager.PERMISSION_GRANTED) {
                isGranted = false
                ActivityCompat.requestPermissions(activity, arrayOf(permission), requestCode)
            }
        }
        return isGranted
    }

    fun convertDateToLong(date: String): Long {
        var dateLong = 0L
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-YYYY hh:mm:ss", Locale.getDefault())
            dateLong = simpleDateFormat.parse(date).time
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dateLong
    }
}