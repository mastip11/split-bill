package id.mastip.splitbill.splitbill.support.dialog

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.squareup.picasso.Picasso
import id.mastip.splitbill.splitbill.R

class BarcodeDialog(context: Context, var id: String): AlertDialog(context) {

    @BindView(R.id.barcodeImageView)
    protected lateinit var barcodeImageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dialog_barcode)
        ButterKnife.bind(this)

        generateBarcode(id)
    }

    fun generateBarcode(id: String) {
        val multiFormatWriter = MultiFormatWriter()
        try {
            val bitMatrix = multiFormatWriter.encode(id, BarcodeFormat.QR_CODE,200,200)
            val barcodeEncoder = BarcodeEncoder()
            val bitmap = barcodeEncoder.createBitmap(bitMatrix)
            barcodeImageView.setImageBitmap(bitmap)
        } catch (e: WriterException) {
            e.printStackTrace()
        }
    }

    @OnClick(R.id.closeTextView)
    internal fun onCloseClicked() {
        dismiss()
    }
}